import 'package:flutter/material.dart';

class MyProvider with ChangeNotifier{
  String _title='';
  String get title=>_title;

 void nameOne(){
    _title='One';
    notifyListeners();
  }

  void nameTwo(){
    _title='Two';
    notifyListeners();
  }
}